#!/usr/bin/env bash
#

# expected vars from env (inject via systemd timer and an env file)
# - ROUTERS         comma separated list of OpenWRT hosts, reachable over ssh)
# - GIT_REPO        the local git repository holding the incremental backups (/!\ unencrypted secrets)
set -euo pipefail

#set -x

mkcd () {
        if (( $# != 1 )); then
            printf 'usage: mkcd <new-directory>\n'
            return 1
        fi
        if [[ ! -d "$1" ]]; then
            command mkdir -p "$1"
        else
            printf '`%s'\'' already exists: cd-ing.\n' "$1"
        fi
        builtin cd "$1"
}

backup () {
    # early return if path doesn't exist on target
    if ! ssh "${1}" test -e "${2}"; then
        echo "${2} doesn't exist on ${1}, skipping..." >&2
        return
    fi
    DST="$(dirname .${2})"
    if [[ ! -d "${DST}" ]] then
        mkdir -p "${DST}"
    fi
    if ! scp ${FLAG} -r ${1}:${2} "${DST}"; then
        if [[ "${FLAG}" == "" ]]; then
            FLAG="-O"
        elif [[ "${FLAG}" == "-O" ]]; then
            FLAG=""
        fi
        scp ${FLAG} -r ${1}:${2} "${DST}"
    fi
    git add ".${2}"
}

FLAG=""

IFS=',' read -ra ROUTERS_ARR <<< "${ROUTERS}"

WD="/tmp/router-backups-wd"
mkcd "${WD}"
# TODO: more checks that this is pointing to the right repo
if ! git pull; then
    git clone ${GIT_REPO} .
fi

for r in ${ROUTERS_ARR[@]}; do
    if [[ -z ${r} ]]; then
        continue
    fi
    echo $r
    mkcd "${WD}/${r}"
    PATHS_TO_BACKUP=$(ssh "${r}" grep -hre '^[^#]' /lib/upgrade/keep.d)
    mapfile -t PATHS <<< "${PATHS_TO_BACKUP}"
    for p in "${PATHS[@]}"; do
        backup ${r} ${p}
        # TODO: encrypt files that end with key or contain the word 'password' ?
    done
done

if git commit -am "Backup $(date '+%d/%m/%Y')"; then
    git push origin main
    echo "Backup complete"
else
    echo "No new changes"
fi
